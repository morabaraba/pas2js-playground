// import {
//   createWorld,
//   Types,
//   defineComponent,
//   defineQuery,
//   addEntity,
//   addComponent,
//   pipe,
// } from 'bitecs'

// TypeScript types are removed in JavaScript
const Vector3 = { x: "f32", y: "f32", z: "f32" };
const Position = defineComponent(Vector3);
const Velocity = defineComponent(Vector3);

const movementQuery = defineQuery([Position, Velocity]);

const movementSystem = (world) => {
  const { time: { delta } } = world;
  const ents = movementQuery(world);
  for (let i = 0; i < ents.length; i++) {
    const eid = ents[i];
    Position.x[eid] += Velocity.x[eid] * delta;
    Position.y[eid] += Velocity.y[eid] * delta;
    Position.z[eid] += Velocity.z[eid] * delta;
  }
  return world;
};

const timeSystem = (world) => {
  const { time } = world;
  const now = performance.now();
  const delta = now - time.then;
  time.delta = delta;
  time.elapsed += delta;
  time.then = now;
  return world;
};

const pipeline = pipe(movementSystem, timeSystem);

const world = createWorld();
world.time = { delta: 0, elapsed: 0, then: performance.now() };

const eid = addEntity(world);
const eid2 = addEntity(world)
addComponent(world, Position, eid);
addComponent(world, Velocity, eid);
addComponent(world, Position, eid2);
addComponent(world, Velocity, eid2);
Velocity.x[eid] = 1.23;
Velocity.y[eid] = 1.23;

setInterval(() => {
  pipeline(world);
}, 16);

const ents = movementQuery(world)
console.log('ents', ents)