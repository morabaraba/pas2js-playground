{
@title(ASCII Top Down Map with Doors)
@author(d-_-b (#2024))
}
program TownMap;

uses webcrt;

const
  mapWidth = 80;
  mapHeight = 25;
  houseCount = 10; // number of houses to draw
  minHouseSize = 3; // minimum size of a house
  maxHouseSize = 10; // maximum size of a house

var
  map: array[1..mapWidth, 1..mapHeight] of char;
  x, y, i, j, k: integer;
  houseX, houseY, houseW, houseH: integer;
  doorX, doorY: integer;
  doorSide: char;
  visited: array[1..mapWidth, 1..mapHeight] of boolean; // to keep track of visited tiles
  blockCount: integer; // to count the number of blocks
  blockX, blockY: array[1..houseCount] of integer; // to store the coordinates of each block
  blockW, blockH: array[1..houseCount] of integer; // to store the width and height of each block

// a procedure to find the blocks by flood fill algorithm
procedure findBlocks(x, y: integer);
begin
  // if the tile is out of bounds or not a house, return
  if (x < 1) or (x > mapWidth) or (y < 1) or (y > mapHeight) or (map[x, y] <> '#') then
    exit;
  // if the tile is already visited, return
  if visited[x, y] then
    exit;
  // mark the tile as visited
  visited[x, y] := true;
  // update the block coordinates and size
  if x < blockX[blockCount] then
    blockX[blockCount] := x;
  if y < blockY[blockCount] then
    blockY[blockCount] := y;
  if x - blockX[blockCount] + 1 > blockW[blockCount] then
    blockW[blockCount] := x - blockX[blockCount] + 1;
  if y - blockY[blockCount] + 1 > blockH[blockCount] then
    blockH[blockCount] := y - blockY[blockCount] + 1;
  // recursively check the adjacent tiles
  findBlocks(x - 1, y);
  findBlocks(x + 1, y);
  findBlocks(x, y - 1);
  findBlocks(x, y + 1);
end;

begin
  randomize;
  // fill the map with empty space
  for x := 1 to mapWidth do
    for y := 1 to mapHeight do
      map[x, y] := ' ';

  // draw some houses
  for i := 1 to houseCount do
  begin
    // generate random coordinates and size for a house
    houseX := random(mapWidth - maxHouseSize) + 1;
    houseY := random(mapHeight - maxHouseSize) + 1;
    houseW := random(maxHouseSize - minHouseSize) + minHouseSize;
    houseH := random(maxHouseSize - minHouseSize) + minHouseSize;
    // draw the house as a rectangle
    for j := houseX to houseX + houseW do
      for k := houseY to houseY + houseH do
        map[j, k] := '#';
  end;

  // initialize the visited array to false
  for x := 1 to mapWidth do
    for y := 1 to mapHeight do
      visited[x, y] := false;

  // initialize the block count to zero
  blockCount := 0;

  // loop through the map and find the blocks
  for x := 1 to mapWidth do
    for y := 1 to mapHeight do
      if (map[x, y] = '#') and (not visited[x, y]) then
      begin
        // increment the block count
        blockCount := blockCount + 1;
        // initialize the block coordinates and size to large values
        blockX[blockCount] := mapWidth;
        blockY[blockCount] := mapHeight;
        blockW[blockCount] := 0;
        blockH[blockCount] := 0;
        // call the findBlocks procedure
        findBlocks(x, y);
      end;

  // loop through the blocks and add doors
  for i := 1 to blockCount do
  begin
    // generate random coordinates and side for an entry door
    doorSide := chr(random(4) + ord('A')); // A for top, B for right, C for bottom, D for left
    case doorSide of
      'A':
      begin
        doorX := random(blockW[i] - 2) + blockX[i] + 1; // avoid the corners
        doorY := blockY[i];
      end;
      'B':
      begin
        doorX := blockX[i] + blockW[i];
        doorY := random(blockH[i] - 2) + blockY[i] + 1; // avoid the corners
      end;
      'C':
      begin
        doorX := random(blockW[i] - 2) + blockX[i] + 1; // avoid the corners
        doorY := blockY[i] + blockH[i];
      end;
      'D':
      begin
        doorX := blockX[i];
        doorY := random(blockH[i] - 2) + blockY[i] + 1; // avoid the corners
      end;
    end;
    // mark the entry door with write('E')
    map[doorX, doorY] := 'E';
    // generate random coordinates and side for an exit door
    repeat
      doorSide := chr(random(4) + ord('A')); // A for top, B for right, C for bottom, D for left
      case doorSide of
        'A':
        begin
          doorX := random(blockW[i] - 2) + blockX[i] + 1; // avoid the corners
          doorY := blockY[i];
        end;
        'B':
        begin
          doorX := blockX[i] + blockW[i];
          doorY := random(blockH[i] - 2) + blockY[i] + 1; // avoid the corners
        end;
        'C':
        begin
          doorX := random(blockW[i] - 2) + blockX[i] + 1; // avoid the corners
          doorY := blockY[i] + blockH[i];
        end;
        'D':
        begin
          doorX := blockX[i];
          doorY := random(blockH[i] - 2) + blockY[i] + 1; // avoid the corners
        end;
      end;
    until map[doorX, doorY] <> 'E'; // make sure the exit door is not the same as the entry door
    // mark the exit door with write('X')
    map[doorX, doorY] := 'X';
  end;

  // draw the map
  for x := 1 to mapWidth do
    for y := 1 to mapHeight do
    begin
      gotoxy(x, y);
      write(map[x, y]);
    end;
  writeln;
  // wait for a key press
  //readkey;
end.

