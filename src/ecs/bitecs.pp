Unit bitecs;

{$MODE ObjFPC}
{$H+}
{$modeswitch externalclass}

interface

uses SysUtils, JS;

Type

      // Forward class definitions
      TIWorld = class(TJSObject);
      TISchema= class(TJSObject);
      TIComponentProp= class(TJSObject);
      TIComponent= class(TJSObject);
      // Forward class definitions
      TArrayByType = Class;
      TComponentType = Class;
      
      TTQueryModifier_Result = Function (world : TIWorld {W}): jsvalue;
      TTQuery_Result = array of Integer;
      TTDeserializer_Result = array of Integer;
      TTypes = class external name 'Object' (TJSObject)
      Public
        i8 : String;
        ui8 : String;
        ui8c : String;
        i16 : String;
        ui16 : String;
        i32 : String;
        ui32 : String;
        f32 : String;
        f64 : String;
        eid : String;
      end;
      
      TType = string; // Restricted values
      TListType = Array[0..1] of JSValue;
      TTypedArray = jsvalue; // Uint8Array | Int8Array | Uint8Array | Uint8ClampedArray | Int16Array | Uint16Array | Int32Array | Uint32Array | Float32Array | Float64Array
      TArrayByType = class external name 'Object' (TJSObject)
      Public
      end;
      
      TDESERIALIZE_MODE = (REPLACE, APPEND, MAP);
      TComponentType = class external name 'Object' (TJSObject)
      Public
      end;
      
      TComponentProp = jsvalue; // TypedArray | (array of TypedArray)
      TComponent = jsvalue; // IComponent | 
      TQueryModifier = Function (c : array of jsvalue): TTQueryModifier_Result;
      TQuery = Function (world : TIWorld {W}; clearDiff : Boolean = true): TTQuery_Result;
      TSystem = Function (world : TIWorld {W}; args : jsvalue {R}): TIWorld {W};
      TSerializer = Function (target : jsvalue): TJSArrayBuffer;
      TDeserializer = Function (world : TIWorld {W}; packet : TJSArrayBuffer; mode : TDESERIALIZE_MODE): TTDeserializer_Result;
      TgetWorldComponents_Result = array of TComponent;
      TgetAllEntities_Result = array of Integer;
      TgetEntityComponents_Result = array of TComponent;
      TdefineSystem_update = Function (world : TIWorld {W}; args : jsvalue {R}): TIWorld {W};
      Tpipe_fns_Item = Function (args : array of JSValue): JSValue;
      Tpipe_fns = array of Tpipe_fns_Item;
      Tpipe_Result = Function (input : JSValue): JSValue;


    Function _Not(c : jsvalue): jsvalue; external name 'bitecs.not';
    Procedure addComponent(world : TIWorld {W}; component : TComponent; eid : Integer; reset : boolean); overload; external name 'bitecs.addComponent';
    Procedure addComponent(world : TIWorld {W}; component : TComponent; eid : Integer); overload; external name 'bitecs.addComponent';
    Function addEntity(world : jsvalue {W}): Integer; external name 'bitecs.addEntity';
    Function Changed(c : jsvalue): jsvalue; external name 'bitecs.Changed';
    Procedure commitRemovals(world : TIWorld {W}); external name 'bitecs.commitRemovals';
    Function createWorld(obj : TIWorld {W}; size : Integer): TIWorld {W}; overload; external name 'bitecs.createWorld';
    Function createWorld: TIWorld {W}; overload; external name 'bitecs.createWorld';
    Function createWorld(obj: TIWorld {W}): TIWorld {W}; overload; external name 'bitecs.createWorld';
    Function createWorld(size : Integer): TIWorld {W}; overload; external name 'bitecs.createWorld';
    //Function defineComponent(schema : T; size : Integer): TComponentType; overload;
    Function defineComponent: TComponentType; overload; external name 'bitecs.defineComponent';
    //Function defineComponent(schema : T): TComponentType; overload;
    Function defineComponent(schema : JSValue; size : Integer): TComponentType; overload; external name 'bitecs.defineComponent';
    Function defineComponent(schema : JSValue): TComponentType; overload; external name 'bitecs.defineComponent';
    Function defineDeserializer(target : jsvalue): TDeserializer; external name 'bitecs.defineDeserializer';
    Function defineQuery(components : array of jsvalue): TQuery; external name 'bitecs.defineQuery';
    Function defineSerializer(target : jsvalue; maxBytes : Integer): TSerializer; overload; external name 'bitecs.defineSerializer';
    Function defineSerializer(target : jsvalue): TSerializer; overload; external name 'bitecs.defineSerializer';
    Function defineSystem(update : TdefineSystem_update): TSystem; external name 'bitecs.defineSystem';
    Procedure deleteWorld(world : TIWorld {W}); external name 'bitecs.deleteWorld';
    Procedure enableManualEntityRecycling(world : TIWorld {W}); external name 'bitecs.enableManualEntityRecycling';
    Function enterQuery(query : TQuery): TQuery; external name 'bitecs.enterQuery';
    Function entityExists(world : TIWorld {W}; eid : Integer): boolean; external name 'bitecs.entityExists';
    Function exitQuery(query : TQuery): TQuery; external name 'bitecs.exitQuery';
    Procedure flushRemovedEntities(world : TIWorld {W}); external name 'bitecs.flushRemovedEntities';
    Function getAllEntities(world : TIWorld {W}): TgetAllEntities_Result; external name 'bitecs.getAllEntities';
    Function getEntityComponents(world : TIWorld {W}; eid : Integer): TgetEntityComponents_Result; external name 'bitecs.getEntityComponents';
    Function getWorldComponents(world : TIWorld {W}): TgetWorldComponents_Result; external name 'bitecs.getWorldComponents';
    Function hasComponent(world : TIWorld {W}; component : TComponent; eid : Integer): boolean; external name 'bitecs.hasComponent';
    Function pipe(fns : jsvalue): Tpipe_Result; external name 'bitecs.pipe'; varargs;
    Procedure registerComponent(world : TIWorld {W}; component : TComponent); external name 'bitecs.registerComponent';
    Procedure registerComponents(world : TIWorld {W}; components : array of TComponent); external name 'bitecs.registerComponents';
    Procedure removeComponent(world : TIWorld {W}; component : TComponent; eid : Integer; reset : boolean); overload; external name 'bitecs.removeComponent';
    Procedure removeComponent(world : TIWorld {W}; component : TComponent; eid : Integer); overload; external name 'bitecs.removeComponent';
    Procedure removeEntity(world : TIWorld {W}; eid : Integer); external name 'bitecs.removeEntity';
    Function removeQuery(world : TIWorld {W}; query : TQuery): TQuery; external name 'bitecs.removeQuery';
    Function resetChangedQuery(world : TIWorld {W}; query : TQuery): TQuery; external name 'bitecs.resetChangedQuery';
    Function resetWorld(world : TIWorld {W}): TIWorld {W}; external name 'bitecs.resetWorld';
    Procedure setDefaultSize(size : Integer); external name 'bitecs.setDefaultSize';
    Procedure setRemovedRecycleThreshold(newThreshold : Integer); external name 'bitecs.setRemovedRecycleThreshold';

  


implementation
end.
