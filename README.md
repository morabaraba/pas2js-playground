# d-_-b's Pascal to JavaScript Playground.
### MIT 2024

#### See the FreePascal `pas2js` [Wiki](https://wiki.freepascal.org/pas2js), [Source](https://gitlab.com/freepascal.org/fpc/pas2js) and [Demos](https://www.freepascal.org/~michael/pas2js-demos/).

Welcome to my pas2js playground, at the moment it is [a website](https://morabaraba.gitlab.io/pas2js-playground) with some examples. Use the above FreePascal `pas2js` resources for more up to date content.

Hopefully projects in the `src` directory can be in the future promoted to [official pas2js demos](https://gitlab.com/freepascal.org/fpc/pas2js/-/tree/main/demo) as they get more mature and useful.

## Set path on Windows

If you installed Lazarus from the website, you can use FPC shipped with it as follows:

```cmd
set PATH=C:\lazarus\pas2js\bin\x86_64-win64;C:\lazarus\fpc\3.2.2\bin\x86_64-win64;%PATH%
```

If you installed FPC using fpcupdeluxe (preferred), you can use FPC as follows:

```cmd
set PATH=C:\lazarus\pas2js\bin\x86_64-win64;C:\fpcupdeluxe\fpc\bin\x86_64-win64;%PATH%
```

## Compile Pascal 2 Javascript

#### See [how to use `pas2js` on the Wiki](https://wiki.freepascal.org/pas2js#How_to_use_pas2js).

Create a `hello.pas` and `hello.html` file as follows:

```sh
# vi hello.pas
program hello;

begin
  Writeln('Hello, world!');
end.
```


```sh
# vi hello.html
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="hello.js"></script>
</head>
<body>
  <script>
    rtl.run();
  </script>
</body>
</html>
```

Now you can compile the Pascal to Javascript:

```sh
pas2js -Jc -Jirtl.js -Tbrowser hello.pas
```
and open `hello.html` in your browser. In the console log of the developer inspect tool you should see `Hello, world!`.

## Use Lazarus IDE

#### See [Lazarus `pas2js` Integration](https://wiki.freepascal.org/lazarus_pas2js_integration).

Once the integration work you can create a new project in the IDE: Project > New Project > Web Browser Application.

## Using Google Closure Compiler

To minify and obscure the Javascript use [Closure Compiler](https://developers.google.com/closure/compiler/docs/gettingstarted_app):

```sh
set PATH=C:\jdk-21.0.2\bin;%PATH%
java closure-compiler.jar --js hello.js --js_output_file hello.out.js
```

In the future it would be preferred to use [Advanced Compilation](https://developers.google.com/closure/compiler/docs/api-tutorial3) but one needs the type definitions of the pascal rtl in jsdoc annotation see [Annotating JavaScript for the Closure Compiler](https://github.com/google/closure-compiler/wiki/Annotating-JavaScript-for-the-Closure-Compiler). 
Maybe there is already `d.ts` definitions one can convert or use something like [Clutz - Closure to TypeScript Declarations (.d.ts) generator.](https://github.com/angular/clutz)(TODO,WIP).

Other interesting reads would be:

- [Leveraging TypeScript declarations in Pas2JS ~ Michaël Van Canney(2022)](https://www.freepascal.org/~michael/articles/typescript/typescript.pdf)
- [TypeScript: Documentation - JSDoc Reference](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html)
- [TS to JSDoc Conversion (github.com/sveltejs)](https://news.ycombinator.com/item?id=35891259)
- [github.com/DefinitelyTyped - The repository for high quality TypeScript type definitions](https://github.com/DefinitelyTyped/DefinitelyTyped)
- [StackOverflow: Is there a typescript definition file for google closure library?](https://stackoverflow.com/questions/20929493/is-there-a-typescript-definition-file-for-google-closure-library)
