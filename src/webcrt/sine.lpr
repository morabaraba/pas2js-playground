{
@title(Ugly ASCII Sine Wave)
@author(d-_-b (#2024))
}
uses webcrt, math;
const
  pi = 3.141592653589793;
  amplitude = 10; // Height of the wave
  frequency = 0.1; // How many cycles per row
  rows = 50; // How many rows to display
  cols = 160; // How many columns to display
var
  x, y: integer; // Loop variables
  angle: real; // Angle in radians
begin
  clrscr; // Clear the screen
  for y := 1 to rows do // For each row
  begin
    angle := y * frequency * 2 * pi; // Calculate the angle
    x := round(amplitude * sin(angle)) + amplitude + 1; // Calculate the x coordinate
    gotoxy(x, y); // Move the cursor
    write('#'); // Draw the wave
  end;
  gotoxy(1, rows + 1); // Move the cursor to the bottom left corner
  writeln();
end.

