#!/bin/bash
index_html=public/index.html
function builddivs()
{

    files=$(find -regex '.*/.*\.html' | sed 's/\.html$/.lpr/g' | while read file; do test -f "$file" && echo $file; done)

    for file in $files; do
        if [[ $file == *backup* ]]; then
            continue
        fi
        href=`echo $file | sed 's/\.lpr$/.html/g'`
        title=`basename $file`
        author=''
        if grep -q "@title" "$file"; then    
            title=`grep "@title" "$file" | sed 's/@title(\(.*\))/\1/'`
        fi
        if grep -q "@author" "$file"; then    
            author=`grep "@author" "$file" | sed 's/@author(\(.*\) (\(.*\)))/<br>Original: <a href="\2">\1<\/a>/'`
        fi
        echo '<div class="col-3-auto"><h5><a href="'$href'" class="link-success link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" >'$title'</a></h5>
            Source: <a href="https://gitlab.com/morabaraba/pas2js-playground/-/blob/main/'$file'">gitlab</a>'$author'<div><br></div></div>'
    done
}

content=`builddivs`
cat index.build.html > $index_html
echo $content >> $index_html
cat index.bottom.html >> $index_html