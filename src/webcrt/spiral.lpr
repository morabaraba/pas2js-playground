{
@title(ASCII Spiral)
@author(d-_-b (#2024))
}
program spiral;

{$mode objfpc}

uses
  sysutils,
  types,
  webcrt;
CONST
  cwidth = 79;
  cheight = 24;
var
  i, rectW, rectH: smallint;
  spiralX, spiralY: smallint;
  angle, radius, rFactor, aFactor: double;
begin
  angle := 0;
  radius := 1;
  rectW:= cwidth div 2;
  rectH:= cheight div 2;
  rFactor := 1.05; // the factor to multiply the radius
  aFactor := 0.1; // the angle to add
  for i := 1 to 180 do
  begin
    spiralX := Trunc(rectW + cos(angle) * radius);
    spiralY := Trunc(rectH + sin(angle) * radius);
    GotoXY(spiralX, spiralY);
    write('#');
    radius := radius * rFactor; // multiply the radius by the factor
    angle := angle + aFactor; // add the angle
  end;
  writeln()//to flush to dom
end.

