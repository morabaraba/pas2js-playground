{
@title(ASCII Sierpiński Triangle)
@author(d-_-b (#2024))
}
program sierpinski_example;

{$mode objfpc}

uses
  sysutils,
  types,
  webcrt;

procedure drawTriangle(x1, y1, x2, y2, x3, y3: smallint);
// draw a triangle with vertices (x1, y1), (x2, y2), (x3, y3)
var
  x, y: smallint;
begin
  // draw the first edge
  x := x1;
  y := y1;
  while x <> x2 do
  begin
    GotoXY(x, y);
    write('#');
    x := x + 1;
  end;
  // draw the second edge
  x := x2;
  y := y2;
  while y <> y3 do
  begin
    GotoXY(x, y);
    write('#');
    y := y + 1;
  end;
  // draw the third edge
  x := x3;
  y := y3;
  while x <> x1 do
  begin
    GotoXY(x, y);
    write('#');
    x := x - 1;
  end;
end;

procedure sierpinski(x1, y1, x2, y2, x3, y3, level: smallint);
// draw a sierpinski triangle with vertices (x1, y1), (x2, y2), (x3, y3) and recursion level
var
  x4, y4, x5, y5, x6, y6: smallint;
begin
  if level = 0 then
  begin
    // base case: draw a triangle
    drawTriangle(x1, y1, x2, y2, x3, y3);
  end
  else
  begin
    // recursive case: divide the triangle into four smaller triangles and remove the central one
    // find the midpoints of the edges
    x4 := (x1 + x2) div 2;
    y4 := (y1 + y2) div 2;
    x5 := (x2 + x3) div 2;
    y5 := (y2 + y3) div 2;
    x6 := (x3 + x1) div 2;
    y6 := (y3 + y1) div 2;
    // draw the three outer triangles with a smaller level
    sierpinski(x1, y1, x4, y4, x6, y6, level - 1);
    sierpinski(x4, y4, x2, y2, x5, y5, level - 1);
    sierpinski(x6, y6, x5, y5, x3, y3, level - 1);
  end;
end;

var
  n: smallint;
begin
  // read the recursion level from the user
  //write('Enter the recursion level: ');
  //readln(n);
  n:= 8;
  // draw the sierpinski triangle with the given level
  sierpinski(1, 1, 79, 1, 40, 24, n);
  writeln()//to flush to dom
end.

