program worldmapv1;

uses
{$IFDEF PAS2JS}
  WebCrt;
{$ELSE}
  Crt;
{$ENDIF}

const
  ScreenWidth = 80;
  ScreenHeight = 25;
  Planet: array[1..15] of string = (
  '#########__---__#########',
  '######.--       --.######',
  '####./             \.####',
  '###/                 \###',
  '##/                   \##',
  '#|                     |#',
  '|                       |',
  '|                       |',
  '|                       |',
  '#|                     |#',
  '##\                   /##',
  '###\                 /###',
  '####~\             /~####',
  '######~--__   __--~######',
  '############---##########'
  );

  Map: array[1..15] of string = (
    ' 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9',
    '. . . . . . # . . . . . . . # . . . . . ',
    '.## . . . . . . . . . . . . . . . . .###',
    '.#. . ####. .#############. . . . .#. .#',
    '. . ##################### . ######. ##.#',
    '. ####################. . . . ########. ',
    '.   ################# . . . . .###### . ',
    '.#####. ############. # . . . . # .   . ',
    '.#######. ########. . . . . . . ##### . ',
    '. .#### . . . .## . . . . . . . .###### ',
    '. .#### # . . . . ##### . . . . . ##### ',
    '. . ##. . . . . . . ### .#. . . . ### . ',
    '. . . . . . . . . . . . . . . . . ##. . ',
    '. .## . . . . . . . . . . . . . . # . . ',
    '.#############. . . . . . . . . . . . . '
  );

type
  TStar = record
    x, y: Integer;
    color: Byte;
  end;

var
  stars: array[1..100] of TStar;

procedure DrawMapToBuffer(tz: Integer; var buffer: array of string);
var
  i, j, col: Integer;
  line: string;
begin
   for i := low(Map) to High(Map) do
  begin
    line := '';
    for j := 0 to 24 do
    begin
      col := (tz + j) mod Length(Map[1]);
      line := line + Map[i][col];
    end;
    buffer[i] := line;
  end;
end;

procedure DrawPlanetToBuffer(var buffer: array of string);
var
  i, j, xOffset, yOffset: Integer;
begin
  xOffset := (Length(buffer[1]) - Length(Planet[1])) div 2;
  yOffset := (Length(buffer) - Length(Planet)) div 2;

  for i := 1 to Length(Planet) do
  begin
    for j := 1 to Length(Planet[i]) do
    if Planet[i][j] <> ' ' then
    begin
      if Planet[i][j] = '#' then
        buffer[yOffset + i][xOffset + j] := ' '
      else
        buffer[yOffset + i][xOffset + j] := Planet[i][j];
    end;
  end;
end;

procedure DisplayBuffer(var buffer: array of string; x, y: Integer);
var
  i: Integer;
begin

  for i := low(buffer) to High(buffer) do
  begin
    GotoXY(x, y + i);
    WriteLn(buffer[i]);
  end;
end;

procedure InitStars;
var
  i: Integer;
begin
  Randomize;
  for i := 1 to 100 do
  begin
    stars[i].x := Random(ScreenWidth) + 1;
    stars[i].y := Random(ScreenHeight) + 1;
    stars[i].color := Random(2); // 0: black, 1: white
  end;
end;

procedure DisplayStars;
var
  i: Integer;
begin
  for i := 1 to 100 do
  begin
    GotoXY(stars[i].x, stars[i].y);
    stars[i].color := Random(2);
    case stars[i].color of
      0: TextColor(Black);
      1: TextColor(White);
    end;
    Write('*');
  end;
end;

procedure LoopMap;
var
  tz: Integer;
  buffer: array[1..16] of string;
begin
  tz := 1;
  InitStars;
  while not KeyPressed do
  begin
    DrawMapToBuffer(tz, buffer);
    DrawPlanetToBuffer(buffer);
    ClrScr;
    //DisplayStars;
    DisplayBuffer(buffer, 40, 10);

    Delay(200);
    tz := tz + 1;
    if tz > Length(Map[1]) then
      tz := 1;
  end;
end;

begin
  ClrScr;
  LoopMap;
end.

