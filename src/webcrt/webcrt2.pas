unit webcrt2;
(******************************************************************************
 * Part of this file have been coped from FPC crt module.
 ******************************************************************************)

{$mode ObjFPC}

interface

uses
  Web, JS;

type
{ all crt unit coordinates are 1-based }
  tcrtcoord = 1..255;
  TProcedure = procedure;
const
{ Foreground and background color constants }
  Black         = 0;
  Blue          = 1;
  Green         = 2;
  Cyan          = 3;
  Red           = 4;
  Magenta       = 5;
  Brown         = 6;
  LightGray     = 7;

{ Foreground color constants }
  DarkGray      = 8;
  LightBlue     = 9;
  LightGreen    = 10;
  LightCyan     = 11;
  LightRed      = 12;
  LightMagenta  = 13;
  Yellow        = 14;
  White         = 15;

{ Add-in for blinking }
  Blink         = 128;

(* ************************************************************************** *)
(* Constant to emulate VGA                                                    *)
(* ************************************************************************** *)
  VGA = $a0000;

(* ************************************************************************** *)
(* Variables to emulate VGA                                                   *)
(* ************************************************************************** *)
var
  Mem: array of Byte;
  _AX,
  _AH,
  _AL,
  _CX,
  _DX,
  _BX : Byte;

procedure Randomize;

procedure ClrScr;

procedure GotoXY(x, y: Integer);

procedure TextColor(Color: Byte);
procedure TextBackground(Color: Byte);

procedure Write(const text: String);
procedure WriteLn(const text: String = '');

procedure AsyncWrap(AsyncMethod: TProcedure);
function ReadLnPromise(var v: string = ''): TJSPromise;
function readln(var v: string = ''): string; async;


(* ************************************************************************** *)
(* Methods to emulate VGA                                                     *)
(* ************************************************************************** *)
procedure FillChar(
  var x: array of Byte;
  from: SizeInt;
  count: SizeInt;
  Value: Byte
);
procedure       geninterrupt (interrupt: byte);

procedure InitScreenBuffer(width, height: Integer);
procedure DrawBuffer;

procedure createWebCrtCanvas();
Procedure PutPixel (x, y, r, g, b, a: Integer);

implementation

Type
  TScreenBuffer = array of array of Char;
var
  vScreenBuffer: TScreenBuffer;
  vCarrotX, vCarrotY, vMaxWidth, vMaxHeight: Integer;
  vTextColor: string = 'white';
  vTextBackground: string =  'black';
  vMCGAEnabled: boolean = false;
  vWebCrtDiv: TJSElement;
  vWebCrtCanvas: TJSHTMLCanvasElement;
  vWebCrtCanvasCtx: TJSCanvasRenderingContext2D;
  vWebCrtCanvasPixel: TJSImageData;
  vWebCrtCanvasPixelData: TJSUint8ClampedArray;



procedure GotoXY(x, y: Integer);
begin
  vCarrotX := x;
  vCarrotY := y;
end;

procedure GotoXYWrite(x, y: Integer; const text: String);
var
  i: Integer;
begin
  for i := 1 to Length(text) do
  begin
    if (vCarrotX + i - 1) < vMaxWidth then
      vScreenBuffer[y, vCarrotX + i - 1] := text[i]
    else if (y + 1) < vMaxHeight then
    begin
      Inc(y);
      vScreenBuffer[y, 0] := text[i];
    end;
  end;
  vCarrotX := x + Length(text);
  vCarrotY := y;
  DrawBuffer();
end;

procedure InitScreenBuffer(width, height: Integer);
var
  i, j: Integer;
begin
  vMaxWidth := width;
  vMaxHeight := height;
  SetLength(vScreenBuffer, height, width);
  for i := 0 to height - 1 do
    for j := 0 to width - 1 do
      vScreenBuffer[i, j] := ' ';
end;

procedure ClrScr;
begin
  InitScreenBuffer(vMaxWidth, vMaxHeight);
  vCarrotX := 0;
  vCarrotY := 0;
end;

procedure Write(const text: String);
begin
  GotoXYWrite(vCarrotX, vCarrotY, text);
end;

procedure WriteLn(const text: String);
begin
  Write(text);
  Inc(vCarrotY);
  vCarrotX := 0;
end;

procedure createWebCrtCanvas();
begin
  // https://webglfundamentals.org/webgl/lessons/webgl-fundamentals.html
  // make webgl context
  vWebCrtCanvas := TJSHTMLCanvasElement(document.createElement('canvas'));
  vWebCrtCanvas.width := 300;
  vWebCrtCanvas.height := 300;
  vWebCrtCanvasCtx := vWebCrtCanvas.getContextAs2DContext('2d');
  vWebCrtCanvasPixel := vWebCrtCanvasCtx.createImageData(1,1); // only do this once per page
  vWebCrtCanvasPixelData := vWebCrtCanvasPixel.data;                        // only do this once per page
  document.body.appendChild(vWebCrtCanvas);
end;

// https://stackoverflow.com/a/4900656
procedure PutPixel(x, y, r, g, b, a: Integer);
begin
vWebCrtCanvasPixelData[0]   := r;
vWebCrtCanvasPixelData[1]   := g;
vWebCrtCanvasPixelData[2]   := b;
vWebCrtCanvasPixelData[3]   := a;
vWebCrtCanvasCtx.putImageData( vWebCrtCanvasPixel, x, y );
end;

procedure Randomize;
begin
  // Empty method to allow normal pascal code to work
end;

procedure DrawBuffer;
begin
  asm
  if (!this.$impl.vScreenBuffer) return;
  this.$impl.vWebCrtDiv.innerHTML = this.$impl.vScreenBuffer.map(row => row.join("")).join("<br>");
  end;
end;

function ColorByte2String(Color: Byte): string;
begin
  case Color of
   0: Result := 'Black';
   1: Result := 'Blue';
   2: Result := 'Green';
   3: Result := 'Cyan';
   4: Result := 'Red';
   5: Result := 'Magenta';
   6: Result := 'Brown';
   7: Result := 'White';
   8: Result := 'Grey';
   9: Result := '#ADD8E6';
   10: Result := '#90ee90';
   11: Result := '#e0ffff';
   12: Result := '#FFCCCB';
   13: Result := '#FF80FF';
   14: Result := 'Yellow';
   15: Result := '#ffffff ';
   else Result := 'black';
 end;
end;

procedure TextColor(Color: Byte);
begin
     vTextColor:= ColorByte2String(Color);
end;

procedure TextBackground(Color: Byte);
begin
  vTextBackground:= ColorByte2String(Color);
end;

procedure AsyncWrap(AsyncMethod: TProcedure);
begin
  asm
    (async () => {
      AsyncMethod()
    })();
  end;
end;

function readln(var v: string): string; async;
begin
  Result := await(string, ReadLnPromise());
end;

function ReadLnPromise(var v: string): TJSPromise;
begin
  Result:=TJSPromise.new(procedure(resolve, reject : TJSPromiseResolver)
    begin
      asm
      let input = ''
      function keydownEvent(event) {
      // Listen for the Enter key press
          if (event.key === 'Enter') {
              document.removeEventListener('keydown', keydownEvent);
              v = input
              resolve(input);
          } else if (event.key.length === 1){
              input += event.key
          }
      }
      document.addEventListener('keydown', keydownEvent);
      end;
    end);
end;


(* ************************************************************************** *)
(* Methods to emulate VGA                                                     *)
(* ************************************************************************** *)
procedure FillChar(var x: array of Byte; from: SizeInt; count: SizeInt;
  Value: Byte);
var
  i: SizeInt;
begin
  for i:= 0 to count-1 do
  begin
    x[from+i] := Value
  end;
end;

procedure geninterrupt(interrupt: byte);
begin
  case interrupt of
    $10: begin
      if (_AX = $0013) and (not vMCGAEnabled) then
      begin
        // TODO
      end
      else if (_AX = $0003) and (vMCGAEnabled) then
      begin

      end;
    end;
  end;
end;

initialization
  begin
  vWebCrtDiv := document.getElementById('webcrt_div');
  if (vWebCrtDiv = nil) then
  begin
    vWebCrtDiv:= document.body;
  end;
  InitScreenBuffer(80,25)
end;

end.

