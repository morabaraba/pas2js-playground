{
@title(Unicode Ant Game)
@author(d-_-b (#2024))
}
program antgame;

{$mode objfpc}

uses
  WebCrt, JS, Classes, SysUtils, Web;

const
  MapWidth = 80;
  MapHeight = 40;
  NumResources = 10;
  MaxAnts = 10;

type
  TAntType = (WorkerAnt, SoldierAnt);
  TResourceType = (Cookie, Chestnut, Water);
  TAnt = record
    X, Y: Integer;
    AntType: TAntType;
    HasResource: Boolean;
    Resource: TResourceType;
  end;
  TResource = record
    X, Y: Integer;
    ResourceType: TResourceType;
  end;

var
  Ants: array of TAnt;
  Resources: array [1..NumResources] of TResource;
  ResourceCollected: array[TResourceType.Cookie .. TResourceType.Water] of Integer;
  clock: integer;
procedure InitializeWorld;
var
  i: Integer;
begin
  // Create the ant hill (home) at a random position
  Ants[0].X := Random(MapWidth);
  Ants[0].Y := Random(MapHeight);
  Ants[0].AntType := WorkerAnt;
  Ants[0].HasResource := False;

    // Generate random resource locations
  for i := 1 to MaxAnts do
  begin
    Ants[i].X := Random(MapWidth);
    Ants[i].Y := Random(MapHeight);
    Ants[i].AntType := TAntType(Random(Ord(High(TAntType)) + 1));
    Ants[i].HasResource := False;
  end;

  // Generate random resource locations
  for i := 1 to NumResources do
  begin
    Resources[i].X := Random(MapWidth);
    Resources[i].Y := Random(MapHeight);
    Resources[i].ResourceType := TResourceType(Random(Ord(High(TResourceType)) + 1));
  end;

  for i := ord(low(TResourceType)) to ord(high(TResourceType)) do
  begin
    ResourceCollected[TResourceType(i)] := 0;
  end;

  clock:= 0;
end;

procedure Update;
var
  AntIndex: Integer;
begin
  clock += 1;
  // Update ant behavior here
  for AntIndex := 1 to High(Ants) do
  begin
    with Ants[AntIndex] do
    begin
      if (AntType = SoldierAnt) and (clock mod 2 = 0) then continue;
      if HasResource then
      begin
        if (X < Ants[0].X) then Inc(X)
        else if (X > Ants[0].X) then Dec(X);
        if (Y < Ants[0].Y) then Inc(Y)
        else if (Y > Ants[0].Y) then Dec(Y);
        if (X = Ants[0].X) and (Y = Ants[0].Y) then
        begin
          HasResource := False;
          ResourceCollected[Resource] += 1;
        end;
      end
      else
      begin
        if (X < Resources[AntIndex].X) then Inc(X)
        else if (X > Resources[AntIndex].X) then Dec(X);
        if (Y < Resources[AntIndex].Y) then Inc(Y)
        else if (Y > Resources[AntIndex].Y) then Dec(Y);
        if (X = Resources[AntIndex].X) and (Y = Resources[AntIndex].Y) then
        begin
          HasResource := True;
          Resource := Resources[AntIndex].ResourceType;
          Resources[AntIndex].X := Random(MapWidth);
          Resources[AntIndex].Y := Random(MapHeight);
          Resources[AntIndex].ResourceType := TResourceType(Random(Ord(High(TResourceType)) + 1));
        end;
      end;
    end;
  end;
end;


procedure Draw;
var
  i: Integer;
begin
  ClrScr;
  // Draw ant hill
  GotoXY(Ants[0].X, Ants[0].Y);
  Write('🏰');

  // Draw ants
  for i := 1 to High(Ants) do
  begin
    GotoXY(Ants[i].X, Ants[i].Y);
    if Ants[i].AntType = SoldierAnt then
      Write('💂')
    else
      Write('🐜');
    if Ants[i].HasResource then
    begin
      GotoXY(Ants[i].X, Ants[i].Y - 1);
      case Ants[i].Resource of
        Cookie: Write('🍪');
        Chestnut: Write('🌰');
        Water: Write('💧');
      end;
    end;
  end;

  // Draw resources
  for i := 1 to NumResources do
  begin
    GotoXY(Resources[i].X, Resources[i].Y);
    case Resources[i].ResourceType of
      Cookie: Write('🍪');
      Chestnut: Write('🌰');
      Water: Write('💧');
    end;
  end;
  GotoXY(1, MapHeight);
  WriteLn('⏳:' + IntToStr(Clock) + ' ' + '🍪:' + IntToStr(ResourceCollected[Cookie]) + ' ' + '🌰:' + IntToStr(ResourceCollected[Chestnut]) + ' ' + '💧:' + IntToStr(ResourceCollected[Water]) + ' ');
end;

procedure GameLoop;
begin
  Update;
  Draw;
end;

begin
  TextBackground(Green);
  SetLength(Ants, 1 + MaxAnts); // Initialize ant hill
  InitializeWorld;
  GameLoop;
  window.setInterval(@GameLoop, 200);
end.

