USES WebCrt2, JS;           (* This has a few nice functions in it, such as the
                       READKEY command.                                 *)

CONST VGA = $a0000;  (* This sets the constant VGA to the segment of the
                       VGA screen.                                      *)

{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure SetMCGA;  { This procedure gets you into 320x200x256 mode. }
BEGIN
  _AX := $0013;
  geninterrupt ($10);
END;


{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure SetText;  { This procedure returns you to text mode.  }
BEGIN
  _AX := $0003;
  geninterrupt ($10);
END;


{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure Cls (Col : Byte);
   { This clears the screen to the specified color }
BEGIN
  Fillchar (Mem, $a0000,64000,col);
END;


{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure INTPutpixel (X,Y : Integer; Col : Byte);
   { This puts a pixel on the screen using interrupts. }
BEGIN
  _AH := $0C;
  _AL := Col;
  _CX := x;
  _DX := y;
  _BX := $01;
  geninterrupt ($10);

END;


{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure TestINTPutpixel;
   { This tests out the speed of the INTPutpixel procedure. }
VAR loop1,loop2 : Integer;
BEGIN
  For loop1:=0 to 319 do
    For loop2:=0 to 199 do
      INTPutpixel (loop1,loop2,Random (256));
  //readln;
 // Cls (0);
END;



{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure MEMPutpixel (X,Y : Integer; Col : Byte);
  { This puts a pixel on the screen by writing directly to memory. }
BEGIN
  Mem [VGA+X+(Y*320)]:=Col;
END;


{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
Procedure TestMEMPutpixel;
   { This tests out the speed of the MEMPutpixel procedure. }
VAR loop1,loop2 : Integer;
BEGIN
  For loop1:=0 to 319 do
    For loop2:=0 to 199 do
      MEMPutpixel (loop1,loop2,Random (256));
  //readln;
  Cls (0);
END;

Procedure TestCanvasPutpixel;
   { This tests out the speed of the MEMPutpixel procedure. }
VAR loop1,loop2 : Integer;
BEGIN
  For loop1:=0 to 319 do
    For loop2:=0 to 199 do
      PutPixel (loop1,loop2,Random (256),Random (256),Random (256),255);
  //readln;
  Cls (0);
END;

procedure main; async;
{ÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ}
BEGIN    (* Of the main program *)
  ClrScr;               { This clears the text Screen (CRT unit) }
  Writeln ('What will happen is that I will clear the screen twice. After');
  Writeln ('each clear screen you will have to hit a key. I will then fill');
  Writeln ('the screen twice with randomlly colored pixels using two different');
  Writeln ('methoods, after each of which you will have to hit a key. I will');
  Writeln ('then return you to text mode.');
  Writeln; Writeln;
  Write ('Hit any kay to continue ...');
  await(readln);
  ClrScr;
  createWebCrtCanvas;
  TestCanvasPutpixel;
  Write ('Hit any kay to continue ...');
  await(readln);
(*
  SetMCGA;
  CLS (32);
  TestINTPutpixel;
  readln;
  CLS (90);
  readln;
  TestINTPutpixel;
  TestMEMPutpixel;
  SetText;
*)
ClrScr;
  Writeln ('All done. This concludes the first sample program in the ASPHYXIA');
  Writeln ('Training series. You may reach DENTHOR under the name of GRANT');
  Writeln ('SMITH on the MailBox BBS, or leave a message to ASPHYXIA on the');
  Writeln ('ASPHYXIA BBS. Get the numbers from Roblist, or write to :');
  Writeln ('             Grant Smith');
  Writeln ('             P.O. Box 270');
  Writeln ('             Kloof');
  Writeln ('             3640');
  Writeln ('I hope to hear from you soon!');
  Writeln; Writeln;
  Write   ('Hit any key to exit ...');
  await(readln);
  ClrScr;
END;     (* Of the main program *)

begin
  AsyncWrap(@main);
  end.
