program chessgame;
{$codepage UTF8}

{$mode objfpc}{$H+}
{$APPTYPE CONSOLE}

uses
  crt
  {$IFDEF WINDOWS}
  , Windows
  {$ENDIF}
  ;

const
  BoardSize = 8;
  Pieces: array[1..6] of UTF8String = ('♙', '♖', '♗', '♕', '♔', '♘');

type
  TPiece = (None, Pawn, Rook, Bishop, Queen, King, Knight);
  TPieceColor = (Dark, Light);
  TBoard = array[1..BoardSize, 1..BoardSize] of record
    Piece: TPiece;
    Color: TPieceColor;
  end;

var
  Board: TBoard;

procedure DrawPiece(x, y: integer; piece: TPiece; color: TPieceColor);
begin
  if (x + y) mod 2 = 0 then
  begin
    TextBackground(Yellow);  // Light blocks
  end
  else
  begin
    TextBackground(Blue);    // Dark blocks
  end;
  if piece <> None then
  begin
    if color = Light then
      TextColor(White)
    else
      TextColor(Black);
    Write(
    Pieces[Ord(piece)]);
  end
  else
    Write(' ');
end;

procedure DrawBoard;
var
  x, y: integer;
begin
  ClrScr;
  for y := 1 to BoardSize do
  begin
    for x := 1 to BoardSize do
    begin
      DrawPiece(x, y, Board[x, y].Piece, Board[x, y].Color);
    end;
    WriteLn;
  end;
end;

procedure InitializeBoard;
var
  x: integer;
begin
  for x := 1 to BoardSize do
  begin
    Board[x, 2].Piece := Pawn;
    Board[x, 2].Color := Light;
    Board[x, 7].Piece := Pawn;
    Board[x, 7].Color := Dark;
  end;

  Board[1, 1].Piece := Rook; Board[1, 1].Color := Light;
  Board[8, 1].Piece := Rook; Board[8, 1].Color := Light;
  Board[1, 8].Piece := Rook; Board[1, 8].Color := Dark;
  Board[8, 8].Piece := Rook; Board[8, 8].Color := Dark;

  Board[2, 1].Piece := Knight; Board[2, 1].Color := Light;
  Board[7, 1].Piece := Knight; Board[7, 1].Color := Light;
  Board[2, 8].Piece := Knight; Board[2, 8].Color := Dark;
  Board[7, 8].Piece := Knight; Board[7, 8].Color := Dark;

  Board[3, 1].Piece := Bishop; Board[3, 1].Color := Light;
  Board[6, 1].Piece := Bishop; Board[6, 1].Color := Light;
  Board[3, 8].Piece := Bishop; Board[3, 8].Color := Dark;
  Board[6, 8].Piece := Bishop; Board[6, 8].Color := Dark;

  Board[4, 1].Piece := Queen; Board[4, 1].Color := Light;
  Board[4, 8].Piece := Queen; Board[4, 8].Color := Dark;

  Board[5, 1].Piece := King; Board[5, 1].Color := Light;
  Board[5, 8].Piece := King; Board[5, 8].Color := Dark;
end;

procedure MovePiece(startX, startY, endX, endY: integer);
begin
  Board[endX, endY].Piece := Board[startX, startY].Piece;
  Board[endX, endY].Color := Board[startX, startY].Color;
  Board[startX, startY].Piece := None;
  Board[startX, startY].Color := Light;
  WriteLn('Moved piece from (', startX, ', ', startY, ') to (', endX, ', ', endY, ')');
end;

begin
  clrscr;

 // {$IFDEF WINDOWS}
 // SetUseACP(true);
  //SetSafeCPSwitching(false);
  SetConsoleCP(CP_UTF8);
  SetConsoleOutputCP(CP_UTF8);

//      SetTextCodePage(Output, CP_UTF8); // utf-8, so there should be no conversion on write
 // {$ENDIF}
  InitializeBoard;
  DrawBoard;
  ReadLn;
end.

