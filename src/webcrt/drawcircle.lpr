{
@title(ASCII Draw Circle)
@author(d-_-b (#2024))
}
program DrawCircle;
uses webcrt, math;

const
  PI = 3.14159265358979323846;
  R = 10; // radius of the circle

var
  x, y, xc, yc: integer;
  angle: real;

begin
  clrscr; // clear the screen
  xc := 40; // center of the circle (x-coordinate)
  yc := 12; // center of the circle (y-coordinate)
  angle := 0;
  while angle < (2 * PI) do // loop through all angles from 0 to 2*pi
  begin
    x := round(xc + R * cos(angle)); // calculate x-coordinate of a point on the circle
    y := round(yc + R * sin(angle)); // calculate y-coordinate of a point on the circle
    gotoxy(x, y); // move the cursor to (x, y)
    write('#'); // write '#' at (x, y)
    angle += 0.01;
  end;
  gotoxy(1, 25); // move the cursor to the bottom-left corner
  writeln;
end.

