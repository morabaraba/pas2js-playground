{
@title(Nibble Entity Component System)
@author(NateTheGreatt/bitECS (https://github.com/NateTheGreatt/bitECS))
}
program nibecs;

{$mode objfpc}

//uses JS, Classes, SysUtils, Web;
uses JS, Web, SysUtils, bitecs;

type
  (*
  performance = class external name 'performance' (TJSObject)
  Public
    Var
      function now(): Double;
  end;
  *)
  TTime = record
    delta, elapsed, last: Double
  end;

  TMyWorld = class(TIWorld)
    time: TTime;
  end;

  TVector3 = record
    x, y, z: string;
  end;

  TVectorComponent = class(TComponentType)
    x, y, z: array of Double;
  end;
var
  Vector3: TVector3 = (x: 'f32'; y: 'f32'; z: 'f32');
  Position,
  Velocity: TVectorComponent;
  movementQuery: TQuery;

  pipeline: Tpipe_Result;
  world: TMyWorld; //TIWorld;
    function movementSystem(world: TIWorld): TIWorld;
  var
    delta: Double;
    i, eid: Integer;
    ents: array of Integer;
  begin
//    delta := world.time.delta;
    ents := movementQuery(world);
    for i := 0 to length(ents)-1 do
    begin
      eid := ents[i];
      Position.x[eid] += Velocity.x[eid] * delta;
      Position.y[eid] += Velocity.y[eid] * delta;
      Position.z[eid] += Velocity.z[eid] * delta;

    end;
    Result:= world;
  end;


function  timeSystem(world: TIWorld): TIWorld;
var
  time: TTime;
  at, delta: Double;
begin
  at := now();//performance.now();
  delta := at - time.last;
  time.delta := delta;
  time.elapsed += delta;
  time.last := at;
  Result:= world;
end;

procedure IntervalProc;
begin
  pipeline(world);
  writeln('world time', world.time.last);
end;
Type

var
  intervalID: NativeInt;

  eid, eid2: Integer;
  ents: array of Integer;

begin
  Position := TVectorComponent(defineComponent(Vector3));
  Velocity := TVectorComponent(defineComponent(Vector3));
  movementQuery := defineQuery([Position, Velocity]);




  pipeline := pipe(@movementSystem, @timeSystem);

  world := TMyWorld(createWorld());
asm
$mod.world.time   = { delta: 0, elapsed: 0, last: performance.now() };
end;
  //world.time = { delta: 0, elapsed: 0, then: performance.now() };

eid := addEntity(world);
eid2 := addEntity(world);
  addComponent(world, Position, eid);
  addComponent(world, Velocity, eid);
  addComponent(world, Position, eid2);
  addComponent(world, Velocity, eid2);
  Velocity.x[eid] := 1.23;
  Velocity.y[eid] := 1.23;
  ents := movementQuery(world);
  intervalID := window.setInterval(@IntervalProc, 100);
end.
