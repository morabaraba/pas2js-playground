unit webcrt;
(******************************************************************************
 * Part of this file have been coped from FPC crt module.
 ******************************************************************************)

{$mode ObjFPC}

interface

uses
  Web, JS;

type
{ all crt unit coordinates are 1-based }
  tcrtcoord = 1..255;
  TProcedure = procedure;
const
{ Foreground and background color constants }
  Black         = 0;
  Blue          = 1;
  Green         = 2;
  Cyan          = 3;
  Red           = 4;
  Magenta       = 5;
  Brown         = 6;
  LightGray     = 7;

{ Foreground color constants }
  DarkGray      = 8;
  LightBlue     = 9;
  LightGreen    = 10;
  LightCyan     = 11;
  LightRed      = 12;
  LightMagenta  = 13;
  Yellow        = 14;
  White         = 15;

{ Add-in for blinking }
  Blink         = 128;

(* ************************************************************************** *)
(* Constant to emulate VGA                                                    *)
(* ************************************************************************** *)
  VGA = $a0000;

(* ************************************************************************** *)
(* Variables to emulate VGA                                                   *)
(* ************************************************************************** *)
var
  Mem: array of Byte;
  _AX,
  _AH,
  _AL,
  _CX,
  _DX,
  _BX : Byte;

procedure Randomize;

procedure ClrScr;

procedure GotoXY(X,Y: tcrtcoord);

procedure TextColor(Color: Byte);
procedure TextBackground(Color: Byte);

procedure Write(astr: string);
procedure Writeln(astr: string='');

procedure AsyncWrap(AsyncMethod: TProcedure);
function ReadLnPromise(var v: string = ''): TJSPromise;
function readln(var v: string = ''): string; async;

(* ************************************************************************** *)
(* Methods to emulate VGA                                                     *)
(* ************************************************************************** *)
procedure FillChar(
  var x: array of Byte;
  from: SizeInt;
  count: SizeInt;
  Value: Byte
);
procedure       geninterrupt (interrupt: byte);


implementation

var
  vbuffer: string = '';
  vTextColor: string = 'white';
  vTextBackground: string =  'black';
  vCloseGotoXY: boolean = false;
  vMCGAEnabled: boolean = false;
  vWebCrtDiv: TJSElement;

procedure Randomize;
begin
  // Empty method to allow normal pascal code to work
end;

procedure ClrScr;
begin
  vCloseGotoXY:= false;
  vbuffer:='';
  vWebCrtDiv.innerHTML :=vbuffer;
end;

procedure GotoXY(X, Y: tcrtcoord);
begin
  vbuffer+='<span style="position: absolute; top: ' + str(Y) + 'em; left: ' + str(X) + 'em">';
  vCloseGotoXY:= true;
end;

function ColorByte2String(Color: Byte): string;
begin
  case Color of
   0: Result := 'Black';
   1: Result := 'Blue';
   2: Result := 'Green';
   3: Result := 'Cyan';
   4: Result := 'Red';
   5: Result := 'Magenta';
   6: Result := 'Brown';
   7: Result := 'White';
   8: Result := 'Grey';
   9: Result := '#ADD8E6';
   10: Result := '#90ee90';
   11: Result := '#e0ffff';
   12: Result := '#FFCCCB';
   13: Result := '#FF80FF';
   14: Result := 'Yellow';
   15: Result := '#ffffff ';
   else Result := 'black';
 end;
end;

procedure TextColor(Color: Byte);
begin
     vTextColor:= ColorByte2String(Color);
end;

procedure TextBackground(Color: Byte);
begin
  vTextBackground:= ColorByte2String(Color);
end;

procedure Write(astr: string);
begin
  vbuffer+='<span style="color:'+ vTextColor +'; background:'+ vTextBackground +'">'+astr+'</span>';
  if(vCloseGotoXY) then begin
    vbuffer+= '</span>';
    vCloseGotoXY := false;
  end;
end;

procedure Writeln(astr: string);
var
  i: smallint;
begin
  vbuffer += '<span style="color:'+ vTextColor +'; background:'+ vTextBackground +'">'+astr + '</span><br>';
  if(vCloseGotoXY) then begin
    vbuffer+= '</span>';
    vCloseGotoXY := false;
  end;
  vWebCrtDiv.innerHTML := document.body.innerHTML + vbuffer;
  vbuffer:='';
end;

procedure AsyncWrap(AsyncMethod: TProcedure);
begin
  asm
    (async () => {
      AsyncMethod()
    })();
  end;
end;

function readln(var v: string): string; async;
begin
  Result := await(string, ReadLnPromise());
end;

function ReadLnPromise(var v: string): TJSPromise;
begin
  Result:=TJSPromise.new(procedure(resolve, reject : TJSPromiseResolver)
    begin
      asm
      let input = ''
      function keydownEvent(event) {
      // Listen for the Enter key press
          if (event.key === 'Enter') {
              document.removeEventListener('keydown', keydownEvent);
              v = input
              resolve(input);
          } else if (event.key.length === 1){
              input += event.key
          }
      }
      document.addEventListener('keydown', keydownEvent);
      end;
    end);
end;


(* ************************************************************************** *)
(* Methods to emulate VGA                                                     *)
(* ************************************************************************** *)
procedure FillChar(var x: array of Byte; from: SizeInt; count: SizeInt;
  Value: Byte);
var
  i: SizeInt;
begin
  for i:= 0 to count-1 do
  begin
    x[from+i] := Value
  end;
end;

procedure geninterrupt(interrupt: byte);
begin
  case interrupt of
    $10: begin
      if (_AX = $0013) and (not vMCGAEnabled) then
      begin
        // TODO
      end
      else if (_AX = $0003) and (vMCGAEnabled) then
      begin

      end;
    end;
  end;
end;

initialization
  begin
  vWebCrtDiv := document.getElementById('webcrt_div');
  if (vWebCrtDiv = nil) then
  begin
    vWebCrtDiv:= document.body;
  end;
end;

end.

