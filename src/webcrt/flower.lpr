{
@title(ASCII Flower)
@author(d-_-b (#2024))
}
uses webcrt, math;
const
  rows = 25; // How many rows to display
  cols = 80; // How many columns to display
  radius = 10; // Radius of the flower
  petals = 5; // Number of petals
var
  x, y: integer; // Loop variables
  angle, theta: real; // Angle in radians
begin
  //clrscr; // Clear the screen
  randomize; // Initialize the random number generator
  for y := 1 to rows do // For each row
  begin
    for x := 1 to cols do // For each column
    begin
      angle := arctan2(y - rows div 2, x - cols div 2); // Calculate the angle from the center
      theta := angle * petals; // Multiply the angle by the number of petals
      if (sqrt(sqr(x - cols div 2) + sqr(y - rows div 2)) <= radius * abs(cos(theta))) then // Check if the point is inside the flower
      begin
        gotoxy(x, y); // Move the cursor
        write('#'); // Draw the flower
      end;
    end;
  end;
  gotoxy(1, rows + 1); // Move the cursor to the bottom left corner
  writeln();
end.

