{$codepage UTF8}
uses
  {$IFDEF PAS2JS}
    WebCrt,
  {$ELSE}
    Crt,
  {$ENDIF}
  {$IFDEF WINDOWS}
    Windows
  {$ENDIF};

type
  TBufferCell = record
    TextColor: Byte;
    BackgroundColor: Byte;
    Character: Char;
    Visible: Boolean;
  end;

var
  ScreenWidth, ScreenHeight: Integer;
  Buffer: array of array of TBufferCell;

procedure InitBuffer;
begin
  SetLength(Buffer, ScreenWidth, ScreenHeight);
end;

procedure ClearBuffer;
var
  x, y: Integer;
begin
  for x := 0 to ScreenWidth - 1 do
    for y := 0 to ScreenHeight - 1 do
    begin
      Buffer[x][y].TextColor := White;
      Buffer[x][y].BackgroundColor := Black;
      Buffer[x][y].Character := ' ';
      Buffer[x][y].Visible := False;
    end;
end;

procedure RandomBuffer(Characters: string);
var
  x, y: Integer;
  CharIndex: Integer;
begin
  Randomize;
  for x := 0 to ScreenWidth - 1 do
    for y := 0 to ScreenHeight - 1 do
    begin
      Buffer[x][y].TextColor := Random(16);
      Buffer[x][y].BackgroundColor := Random(16);
      CharIndex := Random(Length(Characters)) + 1;
      Buffer[x][y].Character := Characters[CharIndex];
      Buffer[x][y].Visible := Random(2) = 1;
    end;
end;


procedure DrawBuffer;
var
  x, y: Integer;
begin
  for x := 0 to ScreenWidth - 1 do
    for y := 0 to ScreenHeight - 1 do
    begin
      if Buffer[x][y].Visible then
      begin
        TextColor(Buffer[x][y].TextColor);
        TextBackground(Buffer[x][y].BackgroundColor);
        GotoXY(x + 1, y + 1);
        Write(Buffer[x][y].Character);
      end;
    end;
end;

procedure InitScreen;
var
  ConsoleInfo: TConsoleScreenBufferInfo;
begin
  {$IFDEF WINDOWS}
  SetConsoleOutputCP(CP_UTF8);
  GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), ConsoleInfo);
  ScreenWidth := ConsoleInfo.srWindow.Right - ConsoleInfo.srWindow.Left + 1;
  ScreenHeight := ConsoleInfo.srWindow.Bottom - ConsoleInfo.srWindow.Top;// + 1;
  {$ELSE}
  {$IFDEF PAS2JS}
  asm
    var width = $(window).width();
    var height = $(window).height();
    pas.Unit1.ScreenWidth = width;
    pas.Unit1.ScreenHeight = height;
  end;
  {$ELSE}
  ScreenWidth := 80;
  ScreenHeight := 25;
  {$ENDIF}
  {$ENDIF}
end;

begin
  InitScreen;
  InitBuffer;
  ClearBuffer;
  //RandomBuffer('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');
  RandomBuffer('♔♕♖♗♘♙♚♛♜♝♞♟🂡🂢🂣🂤🂥🂦🂧🂨🂩🂪🂫🂬🂭🂮🂱🂲🂳🂴🂵🂶🂷🂸🂹🂺🂻🂼🂽🂾🎲🎯🎮🎰');
  DrawBuffer;
  ReadLn;
end.

